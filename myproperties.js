import React from 'react';
import { View, Text, StyleSheet, Dimensions, Button, FlatList, Image } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { Card, ListItem } from 'react-native-elements'
//import { ListItem } from 'react-native-elements';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
const list = [
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'av-timer'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    //... // more items
]

detailedScreen = () => {
    alert('Pressed OK');
}
keyExtractor = (item, index) => index.toString()
renderItem = ({ item }) => (
    <Card
        containerStyle={{ borderRadius: 5 }}
        imageStyle={{ borderColor: 'transparent', borderWidth: 1, borderRadius: 5 }}
        imageWrapperStyle={{}}
        image={require('./propertiyImage.jpg')}
        featuredSubtitle='8 Leads'>
        <Text style={{ marginBottom: 10 }}>
            Hero World Trade Tower{'\n\n'}
            Ambegaon
        </Text>
    </Card>
)
const NewProjects = () => (
    <FlatList
        keyExtractor={keyExtractor}
        data={list}
        renderItem={renderItem}
    />
);

const Rentals = () => (
    <FlatList
        keyExtractor={keyExtractor}
        data={list}
        renderItem={renderItem}
    />
);
const Resale = () => (
    <FlatList
        keyExtractor={keyExtractor}
        data={list}
        renderItem={renderItem}
    />
);
export default class MyProperty extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: 'newProjects', title: 'New Projects(3)' },
            { key: 'rentals', title: 'Rentals(4)' },
            { key: 'resale', title: 'Resale(8)' }

        ],
    }

    renderTabBar = props => {
        return (
            <TabBar
                {...props}
                indicatorStyle={{ backgroundColor: 'black' }}
                style={{ backgroundColor: 'white', color: 'black' }}
                renderLabel={this.renderLabel}
            />
        )
    };
    renderLabel = ({ route, focused }) => (
        <Text style={{ color: 'black' }}>
            {route.title}
        </Text>
    );

    render() {
        return (
            <TabView
                navigationState={this.state}
                renderScene={SceneMap({
                    newProjects: NewProjects,
                    rentals: Rentals,
                    resale: Resale
                })}
                onIndexChange={index => this.setState({ index })}
                renderTabBar={this.renderTabBar}
                initialLayout={{ width: Dimensions.get('window').width }}
                scrollEnabled={true}
                tabStyle={{ width: 600, backgroundColor: 'red' }}

            />
        );
    }
}