import React from 'react';
import { View, Text, StyleSheet, Dimensions, Button,FlatList } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { ListItem } from 'react-native-elements';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
const list = [
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'av-timer'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    {
        title: 'We are using List item and we will integrate flat list with it.testing done.',
        icon: 'flight-takeoff'
    },
    //... // more items
]
detailedScreen=()=>{
    alert('Pressed OK');
}
keyExtractor = (item, index) => index.toString()
renderMatchesItem = ({ item }) => (
    <ListItem
        title={item.title}
        bottomDivider
        chevron
        subtitle='1 july,2019'
        onPress={detailedScreen}
    />
)
const Property = () => (
    <FlatList
        keyExtractor={keyExtractor}
        data={list}
        renderItem={renderMatchesItem}
    />
);

const Leads = () => (
    <FlatList
        keyExtractor={keyExtractor}
        data={list}
        renderItem={renderMatchesItem}
    />
);


export default class MyMatches extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: 'myProperty', title: 'My Properties' },
            { key: 'myLeads', title: 'My Leads' }
        ],
    }

    renderTabBar = props => {
        return (
            <TabBar
                {...props}
                indicatorStyle={{ backgroundColor: 'black' }}
                style={{ backgroundColor: 'white', color: 'black' }}
                renderLabel={this.renderLabel}
            />
        )
    };
    renderLabel = ({ route, focused }) => (
        <Text style={{ color: 'black' }}>
            {route.title}
        </Text>
    );

    render() {
        return (
            <TabView
                navigationState={this.state}
                renderScene={SceneMap({
                    myProperty: Property,
                    myLeads : Leads
                })}
                onIndexChange={index => this.setState({ index })}
                renderTabBar={this.renderTabBar}
                initialLayout={{ width: Dimensions.get('window').width }}
                scrollEnabled={true}
                tabStyle={{ width: 600, backgroundColor: 'red' }}

            />
        );
    }
}
