import React from 'react';
import { View, Text,Button,StyleSheet } from 'react-native';
import { createAppContainer } from 'react-navigation';
import BottomTabMenu from './bottomTabMenu';

import { createDrawerNavigator } from 'react-navigation-drawer';
class MyHomeScreen extends React.Component {
    static navigationOptions = {
      drawerLabel: 'Home',
    };
  
    render() {
      return (
        <Button
          onPress={() => this.props.navigation.navigate('Notifications')}
          title="Go to notifications"
        />
      );
    }
  }
  
  class MyNotificationsScreen extends React.Component {
    static navigationOptions = {
      drawerLabel: 'Notifications',
      
    };
  
    render() {
      return (
        <Button
          onPress={() => this.props.navigation.goBack()}
          title="Go back home"
        />
      );
    }
  }
  
  const styles = StyleSheet.create({
    icon: {
      width: 24,
      height: 24,
    },
  });
  
  const MyDrawerNavigator = createDrawerNavigator({
    Home: {
      screen: MyHomeScreen,
    },
    Notifications: {
      screen: MyNotificationsScreen,
    },
    Tab: BottomTabMenu
  });
  
  //const MyApp = createAppContainer(MyDrawerNavigator);
  export default createAppContainer(MyDrawerNavigator);