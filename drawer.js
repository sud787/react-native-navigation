import { View, StyleSheet } from 'react-native';
import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { Avatar, Drawer, Toolbar } from 'react-native-material-ui';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: 260,
        elevation: 4,
        backgroundColor: 'white',
    },
});

const propTypes = {
    navigation: PropTypes.shape({
        goBack: PropTypes.func.isRequired,
    }).isRequired,
};

class DrawerSpec extends Component {
    state = { drawer: false, login: false }

    drawerToggle = () => {
        this.setState({
            drawer: !this.state.drawer
        })
    }
    render() {
        return (
            <Drawer
                style={styles.container}
                open={false}
                onClick={() => this.drawerToggle()}

            >
                <Drawer.Section
                    divider
                    items={[
                        { icon: 'bookmark-border', value: 'Notifications' },
                        { icon: 'today', value: 'Calendar', active: true },
                        { icon: 'people', value: 'Clients' },
                    ]}
                />
                <Drawer.Section
                    title="Personal"
                    items={[
                        { icon: 'info', value: 'Info' },
                        { icon: 'settings', value: 'Settings' },
                    ]}
                />
            </Drawer>

        );
    }
}

DrawerSpec.propTypes = propTypes;

export default DrawerSpec;