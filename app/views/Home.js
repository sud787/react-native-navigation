import React, { Component } from "react";
import { View,Text } from "react-native";
import BottomTabMenu from '../../bottomTabMenu';

//import CustomTabNavigator from "../components/CustomTabNavigator";
import CustomHeader from "../components/CustomHeader";
import Header from '../../headerMenu';


export default class Home extends Component {
 // static router = CustomTabNavigator.router;
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header navigation={this.props.navigation} />
        <BottomTabMenu />
      </View>
    );
  }
}