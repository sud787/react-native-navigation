import React from "react";
import { View,ScrollView } from "react-native";
import { DrawerItems } from "react-navigation-drawer";
import SafeAreaView from 'react-native-safe-area-view';
import { DrawerNavigatorItems } from 'react-navigation-drawer';
import styles from "./styles";

const CustomDrawerNavigator = props => (
  <ScrollView>
    <SafeAreaView
      style={styles.container}
      forceInset={{ top: 'always', horizontal: 'never' }}
    >
      <DrawerNavigatorItems activeBackgroundColor={"black"}
           activeTintColor={"white"}
           iconContainerStyle={styles.icons}
           {...props} />
    </SafeAreaView>
  </ScrollView>
);
  // <View style={[styles.container]}>
  //   <DrawerItems
  //     activeBackgroundColor={"black"}
  //     activeTintColor={"white"}
  //     iconContainerStyle={styles.icons}
  //     {...props}
  //   />
  // </View>
//);

export default CustomDrawerNavigator;