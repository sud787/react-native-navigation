import React, { Component } from "react";
import { View,Text } from "react-native";
import Header from '../../headerMenu';
import BottomTabMenu from '../../bottomTabMenu';
export default class Settings extends Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "red" }}>
      <Header navigation={this.props.navigation} />
        <Text>setting screen</Text>
        <BottomTabMenu />
      </View>
    );
  }
}