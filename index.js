/**
 * @format
 */

import {AppRegistry} from 'react-native';
//import App from './App';
//import Drawer from './drawer';
//import Header from './headerMenu';
import MainApp from './app/MainApp'
//import { Drawer } from './navigators'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => MainApp);
