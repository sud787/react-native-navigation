import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { Card, Avatar } from 'react-native-elements';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Header from './headerMenu';
import { TabView, SceneMap } from 'react-native-tab-view';
import MyMatches from './matches';
import MyProperty from './myproperties';
const FirstRoute = () => (
  <View  >
    <Text>First Tab</Text>
  </View>
);

const SecondRoute = () => (
  <View>
    <Text>Second  Tab</Text>
  </View>
);
const ThirdRoute = () => (
  <View>
    <Text>thied  Tab</Text>
  </View>
);
const FourthdRoute = () => (
  <View>
    <Text>fourth  Tab</Text>
  </View>
);

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <Card containerStyle={{ borderRadius: 5, height: 70, width: 380 }}>
          <Text  >Hello demob1{'\n\n'}
            Welcome to RE/MAX</Text>
          <Avatar rounded title="MD" />
        </Card>
      </View>

    );
  }
}

class MYLeads extends React.Component {
  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'New Leads' },
      { key: 'second', title: 'Meetings Scheduled' },
      { key: 'third', title: 'Meetings Done' },
      { key: 'fourth', title: 'Booking ' },
    ],
  };

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({
          first: FirstRoute,
          second: SecondRoute,
          third:ThirdRoute,
          fourth:FourthdRoute
        })}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{ width: Dimensions.get('window').width }}
        scrollEnabled={true}
        tabStyle={{width:600,backgroundColor:'red'}} 
      />
    );
  }
}

class MyProfile extends React.Component {
  render() {
    return (
      <View>

        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>Profile</Text>
        </View>
      </View>
    );
  }
}
const AppNavigator = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        title: "Home",
        tabBarLabel: "Home",
        tabBarIcon: <Icon size={22} color="grey" name="home" />
      }
    },
    Matches: {
      screen: MyMatches,
      navigationOptions: {
        title: "Matches",
        tabBarLabel: "Matches",
        tabBarIcon: <Icon size={22} color="grey" name="bell" />
      }
    },
    Leads: {
      screen: MYLeads,
      navigationOptions: {
        title: "Leads",
        tabBarLabel: "Leads",
        tabBarIcon: <Icon size={22} color="grey" name="account-multiple" />
      }
    },
    Property: {
      screen: MyProperty,
      navigationOptions: {
        title: "Property",
        tabBarLabel: "Property",
        tabBarIcon: <Icon size={22} color="grey" name="apps" />
      }
    },
    Profile: {
      screen: MyProfile,
      navigationOptions: {
        title: "More",
        tabBarLabel: "More",
        tabBarIcon: <Icon size={22} color="grey" name="dots-horizontal" />
      }
    }
  },
  {
    initialRouteName: 'Home',
    activeColor: '#f0edf6',
    barStyle: { backgroundColor: 'white' },
    //labeled: false
  }
);


//export default AppNavigator;
export default createAppContainer(AppNavigator);