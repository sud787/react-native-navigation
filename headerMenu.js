import React from 'react';
import { StyleSheet, Text, View ,Container} from 'react-native';
import { Toolbar,Badge } from 'react-native-material-ui';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import Drawer from './drawer';

export default class HeaderMenu extends React.Component {
    state = {
        search: ''
    };
    updateSearch = search => {
        this.setState({ search });
    };
    render() {
        const { search } = this.state;
        return (
            <Toolbar style={{ container: { backgroundColor: 'white' }, 
            leftElement: { color: 'black' },
            rightElement: { color: 'black' },
            centerElementContainer:{justifyContent: 'center', },
            titleText:{ color: 'black' },
         }}
            leftElement="menu"
            onLeftElementPress={() => this.props.navigation.toggleDrawer()}
            centerElement="Real MAX"
            rightElement={<Badge text="3" style={{container:{right:3,backgroundColor: 'red',top:-4} }}>
            <Icon name="bell" size={22}  style={{right:12 }} />
          </Badge>}
            
            />
                )
            }
        }
        
